const express = require('express');
//const session = require('express-session');
const router = express.Router();

const {
    Student
} = require('../models/student');

const {
    Faculty
} = require('../models/faculty');

const {
    CreateGroup
} = require('../models/create-group');

const {
    CreateTest
} = require('../models/create-test');

const {
    SaveTest
} = require('../models/save-test');

const {
    JoinGroup
} = require('../models/join-group');

const {
    isStudentLoggedIn,
    isFacultyLoggedIn,
    isLoggedIn
} = require('../config/auth');


// Teachers Routes.

// Home Route
router.get('/', isLoggedIn, (req, res) => {
    res.render('index');
});

// Faculty Routes.
// Faculty Login
router.get('/faculties/login', isLoggedIn, (req, res) => {
    res.render('faculties/login', {
        title: 'Faculty | Login'
    });
});

router.post('/faculties/login', isLoggedIn, async (req, res) => {
    let username = req.body.name;
    let password = req.body.password;

    const result = await Faculty.findOne({
        name: username,
        password: password
    });

    if (result) {
        req.session.faculty = result;
        req.flash('success_msg', 'Login Successfull.');
        res.redirect('/faculties');
    } else {
        req.flash('error_msg', 'Invalid username / password.');
        res.redirect('/faculties/login');
    }
});

// Faculty Logout
router.get('/faculties/logout', isFacultyLoggedIn, function (req, res) {
    req.flash('success_msg', 'You have successfully logged out.');
    req.session.destroy(function () {
        console.log("You have successfylly logged out.")
    });
    res.redirect('/');
});


// Faculties Index Route
router.get('/faculties', isFacultyLoggedIn, (req, res) => {
    res.render('faculties/dashboard');
});

// Add Student
router.get('/faculties/add-student', isFacultyLoggedIn, (req, res) => {
    res.render('faculties/add-student', {
        title: 'Add Student'
    });
});

router.post('/faculties/add-student', isFacultyLoggedIn, async (req, res) => {
    const student = new Student({
        name: req.body.stdName,
        classRollNo: req.body.stdRoll
    });

    const stud = await Student.findOne({
        classRollNo: req.body.stdRoll
    });

    if (stud) {
        req.flash('error_msg', 'Roll number already exists.');
        res.redirect('/faculties/add-student');
    } else {
        const result = await student.save();

        if (result) {
            req.flash('success_msg', 'Student added successfully.');
            res.redirect('/faculties/add-student');
        }
    }
});

// Add Faculty
router.get('/faculties/add-faculty', isFacultyLoggedIn, (req, res) => {
    res.render('faculties/add-faculty', {
        title: 'Add New Faculty'
    });
});

router.post('/faculties/add-faculty', isFacultyLoggedIn, async (req, res) => {
    const faculty = new Faculty({
        name: req.body.name,
        password: req.body.password
    });

    const result = await faculty.save();

    if (result) {
        req.flash('success_msg', 'Faculty added successfully.');
        res.redirect('/faculties/add-faculty');
    } else {
        req.flash('error_msg', 'Error adding faculty.');
        res.redirect('/faculties/add-faculty');
    }
});


// Create Groups
router.get('/faculties/create-groups', isFacultyLoggedIn, (req, res) => {
    res.render('faculties/create-groups');
})

router.post('/faculties/create-groups', isFacultyLoggedIn, async (req, res) => {

    const group = new CreateGroup({
        groupName: req.body.groupName,
        stud1: req.body.stud1,
        pass1: req.body.pass1,
        stud2: req.body.stud2,
        pass2: req.body.pass2,
        stud3: req.body.stud3,
        pass3: req.body.pass3,
        stud4: req.body.stud4,
        pass4: req.body.pass4
    });

    const result = await group.save();

    if (result) {
        req.flash('success_msg', 'Group Created Successfully.');
        res.redirect('/faculties/create-groups');
    }
});

router.get('/faculties/create-test', isFacultyLoggedIn, (req, res) => {
    res.render('faculties/create-test');
});

router.post('/faculties/create-test', isFacultyLoggedIn, async (req, res) => {

    const test = new CreateTest({
        testName: req.body.testName,
        groupName: req.body.groupName,
        'ques1.q1': req.body.ques1,
        'ques1.q1opt1': req.body.q1opt1,
        'ques1.q1opt2': req.body.q1opt2,
        'ques1.q1opt3': req.body.q1opt3,
        'ques1.q1opt4': req.body.q1opt4,
        'ques1.q1correctOpt': req.body.q1correctOpt,
        'ques2.q2': req.body.ques2,
        'ques2.q2opt1': req.body.q2opt1,
        'ques2.q2opt2': req.body.q2opt2,
        'ques2.q2opt3': req.body.q2opt3,
        'ques2.q2opt4': req.body.q2opt4,
        'ques2.q2correctOpt': req.body.q2correctOpt,
        'ques3.q3': req.body.ques3,
        'ques3.q3opt1': req.body.q3opt1,
        'ques3.q3opt2': req.body.q3opt2,
        'ques3.q3opt3': req.body.q3opt3,
        'ques3.q3opt4': req.body.q3opt4,
        'ques3.q3correctOpt': req.body.q3correctOpt,
        'ques4.q4': req.body.ques4,
        'ques4.q4opt1': req.body.q4opt1,
        'ques4.q4opt2': req.body.q4opt2,
        'ques4.q4opt3': req.body.q4opt3,
        'ques4.q4opt4': req.body.q4opt4,
        'ques4.q4correctOpt': req.body.q4correctOpt
    });

    const result = await test.save();

    if (result) {
        req.flash('success_msg', 'Test Created Successfully.');
        res.redirect('/faculties/create-test');
    }
});

// Check Results
router.get('/faculties/check-result', isFacultyLoggedIn, async (req, res) => {
    const result = await SaveTest.find();

    if (result) {
        res.render('faculties/check-result', {
            title: 'Check Results',
            test: result
        });
    }
});

router.get('/faculties/check', isFacultyLoggedIn, async (req, res) => {
    const result = await SaveTest.findOne({
        _id: req.query.id
    });

    if (result) {
        res.render('faculties/check', {
            title: 'Check Submission',
            result: result
        });
    }
});


// Students Route
router.get('/students', isStudentLoggedIn, (req, res) => {
    res.render('students/dashboard', {
        title: 'Student Dashboard',
        //user: req.session.user
    });
});

router.get('/students/take-tests', isStudentLoggedIn, async (req, res) => {
    const JSONObj = [];

    const groupNames = await JoinGroup.find({
        studentName: req.session.student.name
    }).select({
        group_joined: 1,
        _id: 0
    });

    for (i in groupNames) {
        let r = await CreateTest.findOne({
            groupName: groupNames[i].group_joined
        }).select({
            testName: 1
        });
        //console.log(r.testName);
        let test = {
            "id": r._id,
            "testName": r.testName
        }
        JSONObj.push(test);
    }

    console.log(JSONObj);

    if (JSONObj) {
        res.render('students/take-tests', {
            title: 'Take Tests',
            test: JSONObj
        });
    }
});

router.get('/students/test', isStudentLoggedIn, async (req, res) => {
    const id = req.query.id;

    const test = await CreateTest.findOne({
        _id: id
    });

    if (test) {
        res.render('students/test', {
            title: 'Start Test',
            test: test
        });
    }
});

router.get('/students/groups', isStudentLoggedIn, async (req, res) => {
    const group = await CreateGroup.find();

    if (group) {
        res.render('students/groups', {
            title: 'Groups',
            group: group
        });
    }
});

router.post('/students/groups', isStudentLoggedIn, async (req, res) => {
    const joinGroup = new JoinGroup({
        studentName: req.session.student.name,
        group_joined: req.query.groupName
    });

    const result = await joinGroup.save();

    if (result) {
        req.flash('success_msg', 'You have successfully joined the group.');
        res.redirect('/students/groups');
    }
});

router.post('/students/test', isStudentLoggedIn, async (req, res) => {

    const test = new SaveTest({
        studName: req.session.student.name,
        testName: req.body.testName,
        select_opt1: req.body.q1opt1,
        select_opt2: req.body.q2opt1,
        select_opt3: req.body.q3opt1,
        select_opt4: req.body.q4opt1
    });

    const result = await test.save();

    if (result) {
        req.flash('success_msg', 'Test submitted successfully.');
        res.redirect('/students');
    } else {
        req.flash('error_msg', 'Error submitting test');
        res.redirect('/students/test');
    }
});

router.get('/students/login', isLoggedIn, (req, res) => {
    res.render('students/login', {
        title: 'Student | Login'
    });
});

// Login Form POST
router.post('/students/login', isLoggedIn, async (req, res) => {
    const username = req.body.studentName;
    const password = req.body.studentRoll;

    const student = await Student.findOne({
        name: username,
        classRollNo: password
    });

    if (student) {
        req.session.student = student;
        res.redirect('/students');
    } else {
        req.flash('error_msg', 'Incorrect username / password.');
        res.redirect('/students/login');
    }
});

// Student Logout
router.get('/students/logout', isStudentLoggedIn, function (req, res) {
    req.flash('success_msg', 'You have successfully logged out.');
    req.session.destroy(function () {
        console.log("You have successfylly logged out.")
    });
    res.redirect('/');
});

module.exports = router;