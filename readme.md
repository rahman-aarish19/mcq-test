# Online Test Application

A Full Stack Web application for conducting online tests.

# Features!

  - Profile based Login and Registration.
  - Creating groups for students by teachers / faculties.
  - Create tests for the groups.
  - Attempting test by students.
  - Submitting results to the teachers.
  - Adding new students.
  - Adding new teachers / faculties.
  - etc.

### Technology Stack

This web application uses a number of open source projects to work properly which are mentioned below:

* Html, Html5
* CSS3
* JavaScript
* Twitter Bootstrap 4
* Node.js
* Express.js
* MongoDB

### Installation

This web application requires node.js version 8 or higher to run.

Install the following dependencies and dev dependencies and start the server.

```sh
$ cd MCQ-Test
$ npm i --save express express-handlebars body-parser connect-flash cookie-parser express-session mongoose
$ start
```
### Plugins

This web app uses the following plugins

| Plugin | README |
| ------ | ------ |
| bootstrap.min.css | [https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css][PlDb] |
| bootstrap.min.js | [https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js][PlGh] |
| jquery.min.js | [https://code.jquery.com/jquery-3.3.1.slim.min.js][PlGd] |


Note
-----
You need to insert atleast one faculty data in the faculty collection through MongoDB to access the application because all the routes are protected except the Home route i.e. index

License
----

MIT