const mongoose = require('mongoose');

const saveTestSchema = new mongoose.Schema({
    studName: {
        type: String,
        required: true
    },
    testName: {
        type: String,
        required: true
    },
    select_opt1: {
        type: String,
        required: true
    },
    select_opt2: {
        type: String,
        required: true
    },
    select_opt3: {
        type: String,
        required: true
    },
    select_opt4: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});

const SaveTest = mongoose.model('SaveTest', saveTestSchema);

exports.SaveTest = SaveTest;