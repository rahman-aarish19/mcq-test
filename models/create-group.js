const mongoose = require('mongoose');

const createGroupSchema = new mongoose.Schema({
    groupName: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    stud1: {
        type: String,
        required: true
    },
    pass1: {
        type: String,
        required: true
    },
    stud2: {
        type: String,
        required: true
    },
    pass2: {
        type: String,
        required: true
    },
    stud3: {
        type: String,
        required: true
    },
    pass3: {
        type: String,
        required: true
    },
    stud4: {
        type: String,
        required: true
    },
    pass4: {
        type: String,
        required: true
    }
});

const CreateGroup = mongoose.model('CreateGroup', createGroupSchema);

exports.CreateGroup = CreateGroup;