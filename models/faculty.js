const mongoose = require('mongoose');

const facultySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        default: 'Aarish'
    },
    password: {
        type: String,
        required: true,
        default: 1234
    },
    profile: {
        type: String,
        default: 'Faculty'
    }
});

const Faculty = mongoose.model('Faculty', facultySchema);

exports.Faculty = Faculty;