const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    studentName: {
        type: String,
        required: true
    },
    group_joined: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});

const JoinGroup = mongoose.model('JoinGroup', schema);

exports.JoinGroup = JoinGroup;