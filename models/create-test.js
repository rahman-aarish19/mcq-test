const mongoose = require('mongoose');

const createTestSchema = new mongoose.Schema({
    testName: {
        type: String,
        required: true
    },
    groupName: {
        type: String,
        required: true
    },
    ques1: {
        q1: {
            type: String,
            required: true
        },
        q1opt1: {
            type: String,
            required: true
        },
        q1opt2: {
            type: String,
            required: true
        },
        q1opt3: {
            type: String,
            required: true
        },
        q1opt4: {
            type: String,
            required: true
        },
        q1correctOpt: {
            type: String,
            required: true
        }
    },
    ques2: {
        q2: {
            type: String,
            required: true
        },
        q2opt1: {
            type: String,
            required: true
        },
        q2opt2: {
            type: String,
            required: true
        },
        q2opt3: {
            type: String,
            required: true
        },
        q2opt4: {
            type: String,
            required: true
        },
        q2correctOpt: {
            type: String,
            required: true
        }
    },
    ques3: {
        q3: {
            type: String,
            required: true
        },
        q3opt1: {
            type: String,
            required: true
        },
        q3opt2: {
            type: String,
            required: true
        },
        q3opt3: {
            type: String,
            required: true
        },
        q3opt4: {
            type: String,
            required: true
        },
        q3correctOpt: {
            type: String,
            required: true
        }
    },
    ques4: {
        q4: {
            type: String,
            required: true
        },
        q4opt1: {
            type: String,
            required: true
        },
        q4opt2: {
            type: String,
            required: true
        },
        q4opt3: {
            type: String,
            required: true
        },
        q4opt4: {
            type: String,
            required: true
        },
        q4correctOpt: {
            type: String,
            required: true
        }
    }
});

const CreateTest = mongoose.model('CreateTest', createTestSchema);

exports.CreateTest = CreateTest;