const mongoose = require('mongoose');

const studentSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    classRollNo: {
        type: String,
        required: true,
        unique: true
    },
    profile: {
        type: String,
        default: 'Student'
    }
});

const Student = mongoose.model('Student', studentSchema);

exports.Student = Student;