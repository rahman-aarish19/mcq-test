const session = require('express-session');

module.exports = {
    isStudentLoggedIn: function (req, res, next) {
        if (req.session.student) {
            return next();
        } else if (req.session.faculty) {
            req.flash('error_msg', 'Restricted area! Accessable only to students.');
            res.redirect('/faculties');
        }
        req.flash('error_msg', 'Please login to continue...');
        res.redirect('/students/login');
    },
    isFacultyLoggedIn: function (req, res, next) {
        if (req.session.faculty) {
            return next();
        } else if (req.session.student) {
            req.flash('error_msg', 'Restricted area! Accessable only to faculties.');
            res.redirect('/students');
        }
        req.flash('error_msg', 'Please login to continue...');
        res.redirect('/faculties/login');
    },
    isLoggedIn: function (req, res, next) {
        if (req.session.student) {
            req.flash('error_msg', 'You are already logged in.');
            res.redirect('/students');
        } else if (req.session.faculty) {
            req.flash('error_msg', 'You are already logged in.');
            res.redirect('/faculties');
        } else {
            return next();
        }
    }
}