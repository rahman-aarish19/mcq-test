const express = require('express');
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');
const path = require('path');
const flash = require('connect-flash');
const session = require('express-session');
const cookieParser = require('cookie-parser');

// Loading Routes
const Routes = require('./routes/route');
//const studentsRoute

const app = express();

const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/mcq-test', {
    useNewUrlParser: true
}).then(() => console.log('Connected to MongoDB Server...')).catch(err => console.error('Error occured connecting to MongoDB...', err));

// Express Handlebars Middleware.

app.engine('handlebars', exphbs({
    defaultLayout: 'main'
}));

app.set('view engine', 'handlebars');

// Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser Middleware
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());

// Cookie Parser Middleware
app.use(cookieParser());

// Express Session Middleware
app.set('trust proxy', 1) // trust first proxy
app.use(session({
    secret: 'secret',
    resave: false,
    saveUninitialized: true
}));

// Connect-Flash
app.use(flash());

// Global Variables
app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.faculty = req.session.faculty || null;
    res.locals.student = req.session.student || null;
    next();
});

// Use Routes
app.use('/', Routes);

const port = process.env.NODE_ENV || 3000;

app.set('port', port);

app.listen(port, () => {
    console.log(`Server running on port : ${port}`);
})